<?php

include_once drupal_get_path('module', 'imageset') .'/imageset.inc';

function _menuicon_icon_edit($info = NULL, $menuitem = NULL, $path = TRUE) {

  $form = array();
  if ($path) {
    $form['menuicon_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#default_value' => $info['path'],
      '#description' => t('Path menu for icon. For use a root it is necessary to specify "/"'),
      '#required' => TRUE, 
    );
  }
  $form['menuicon_path_original'] = array(
    '#type' => 'value',
    '#value' => $info['path'],
  );    
  $form['menuicon_name'] = array(
    '#type' => 'select',
    '#title' => t('Imageset'),
    '#default_value' => $info['name'],
    '#options' => array_merge(array('0' => t('<Not use>')), imageset_names()),
  );
  $form['menuicon_index'] = array(
    '#type' => 'textfield',
    '#title' => t('Index'),
    '#default_value' => $info['index'],
    '#description' => t(''),
    '#size' => 4,
  );
  $form['menuicon_type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#default_value' => $info['type'],
    '#description' => t(''),
    '#options' => menuicon_types(),
  );
  $form['menuicon_inherit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Inherit'),
    '#default_value' => $info['inherit'],
    '#description' => t('To inherit for enclosed. For example the icon node/add will be used for all enclosed ways: node/add/page, node/add/news, node/add/*'),
  );
  if ($menuitem) {
    $form['menuicon_item'] = array(
      '#type' => 'item',
      '#title' => t('Sample'),
      '#value' => theme('menuicon_item_link', $menuitem),
    );
  }
  return $form;
}

/**
 * Implementation of hook_form_alter(). 
 Drupal5: function hook_form_alter($form_id, &$form) {
 Drupal6: function hook_form_alter(&$form, $form_state, $form_id) { 
 */
function menuicon_form_alter(&$arg1, &$arg2, &$arg3 = NULL) {

  if (_pvb_form_alter_header($arg1, $arg2, $arg3, (_pvb_is_v6()? 'menu_edit_item': 'menu_edit_item_form'), $form)) {
    if (_pvb_is_v6()) {
      $item = $form['menu']['original_item']['#value'];
    } 
    else {
      $mid = $form['mid']['#value'];
      $item = menu_get_item($mid);
    }
    $path = (_pvb_is_v6() ? $item['href'] : $item['path']);
    $info = _menuicon_linkinfo($path, FALSE);
    $collapsed = !isset($info);
    $info['path'] = $path;
    
    $menuicon = array_merge(
      array(
        '#type' => 'fieldset',
        '#title' => t('Menu icon'),
        '#collapsible' => TRUE,
        '#collapsed' => $collapsed,
      ),
      _menuicon_icon_edit($info, $item, FALSE)
    );
    
    $result = array();
    foreach ($form as $key => $value) {
      if ($key == 'submit') {
        $result['menuicon'] = $menuicon;
      }
      $result[$key] = $value;
    }
    $result['#submit'] = array_merge($result['#submit'], _pvb_form_func('menuicon_item_save_submit'));
    
    if (_pvb_is_v6()) $arg1 /* $form*/ = $result;
      else $arg2 /* $form*/ = $result;
  } 
}

function menuicon_item_save_submit($form_id, &$form_values) {

  $values = _pvb_form_value($form_values);
  $path = (_pvb_is_v6()? $values['menu']['original_item']['path'] : $values['menuicon_path_original']);
  $values['menuicon_path'] = (_pvb_is_v6()? $values['menu']['link_path']: $values['path']);
  menuicon_item_save($path, $values);
}

function menuicon_item_save($path, &$values) {

  $is_new = (empty($path));
  if ($is_new) $path = $values['menuicon_path'];
  $path = ($path && ($path != '<front>') ? $path : '/');
  if ($values['menuicon_name'] || $values['menuicon_inherit']) {
    $info = array();
    $info['newpath'] = drupal_get_normal_path($values['menuicon_path']);
    $info['newpath'] = ($info['newpath'] && ($info['newpath'] != '<front>') ? $info['newpath']: '/');
    $info['name'] = $values['menuicon_name'];
    $info['index'] = $values['menuicon_index'];
    $info['type'] = $values['menuicon_type'];
    $info['inherit'] = $values['menuicon_inherit'];
  
    _menuicon_linkinfo_update($path, $info);
  }
  else if (!$is_new) _menuicon_linkinfo_update($path); //delete
}

function menuicon_admin_settings() {

  $settings = menuicon_settings();
  if (_pvb_is_v6() && isset($_POST['menuicon_settings'])) {
    // clear themed cache for Drupal6
    if ($settings['std_menu_item_link'] != $_POST['menuicon_settings']['std_menu_item_link']) {
      drupal_rebuild_theme_registry();
    }
  }

  $form = array();
  $form['menuicon_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Settings menuicon',
    '#tree' => TRUE,
    '#collapsible' => FALSE,
  );
  $form['menuicon_settings']['define_mic_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Defined menuicon block'),
    '#default_value' => $settings['define_mic_block'],
    '#description' => t('In section <a href="!blocks">blocks</a> it will be possible to operate blocks of the menu with a mark \'menuicon\'', 
      array('!blocks' => url('admin/build/block'))),
  );
  
  $override = _theme_menu_item_link_override(TRUE);
  $form['menuicon_settings']['std_menu_item_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('To redefine items of the standard menu'),
    '#default_value' => ($settings['std_menu_item_link'] && $override),
    '#description' => t('Redefinition will be executed themed \'menu_item_link\''),
    '#disabled' => !$override,
  );
  if (!$override && !_pvb_is_v6()) {
    $form['menuicon_settings']['msg_menu_item_link'] = array(
      '#type' => 'item',
      '#value' => t('Now function phptemplate_menu_item_link is already redefined in your theme. To use there is no opportunity.'),
    );
  }
  return system_settings_form($form);
}


function menuicon_admin_settings_icons() {

  $settings = variable_get('menuicon_index', array());

  $header = array(t('Path'), t('Imagesets'), t('Inherit'), t('Icons'), t('Operation') );
  $rows = array();
  foreach ($settings as $path => $value) {
    $menuicon_info = _menuicon_explodelink($value);
    $rows[] = array(
      check_plain($path),
      ($menuicon_info['name']? check_plain(sprintf('%s:%d', $menuicon_info['name'], $menuicon_info['index'])): "&nbsp;"),
      ($menuicon_info['inherit']? "yes" : "&nbsp;"),
      theme("imageset", $menuicon_info['name'], $menuicon_info['index']),
      _pvb_l(t('Edit'), PATH_IMAGESET_SETTINGS .'/menuicon-icons/edit', array('query' => "key=$path")),
    );
  }

  $form = array();
  $form['new'] = array_merge(
    array(
      '#type' => 'fieldset',
      '#title' => t('New menu icon'),
      '#collapsible' => TRUE,
      '#collapsed' => (count($rows) > 0),
    ), _menuicon_icon_edit()
  );
  $form['new']['submit'] = array('#type' => 'submit', '#value' => t('Save new menu icon') );
  $form['rows'] = array(
    '#type' => 'item',
    '#value' => (count($rows)? theme('table', $header, $rows): t('Not defined values')),
  );

  return $form;
}

function menuicon_admin_settings_icons_submit($form_id, $form_values) {

  $values = _pvb_form_value($form_values);
  $path = (isset($values['menuicon_path_original'])? $values['menuicon_path_original']: $values['menuicon_path']);
  menuicon_item_save($path, $values);
}

function menuicon_icon_edit() {

  $path = isset($_GET['key']) ? $_GET['key'] : '';
  $info = _menuicon_linkinfo($path, FALSE);
  if (!isset($info)) {
    drupal_set_message(t('Path "@path" not found', array('@path' => $path)));
    return;
  }
  $info['path'] = $path;

  $form = array();
  $form['#redirect'] = PATH_IMAGESET_SETTINGS .'/menuicon-icons';
  $form['#submit'] = _pvb_form_func('menuicon_admin_settings_icons_submit');
  $form['edit'] = array_merge(
    array(
      '#type' => 'fieldset',
      '#title' => t('Menu icon'),
      '#collapsible' => FALSE,
    ),
    _menuicon_icon_edit($info)
  );
  $form['edit']['submit'] = array('#type' => 'submit', '#value' => t('Save menu icon') );
  return $form;
}
