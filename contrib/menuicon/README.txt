
Install
-----------------------------------------------
1. Decompress the file into your Drupal modules directory (usually sites/all/modules).
2. Enable the Menuicon module (This module depends on module Imageset):
    Administer > Site building > Modules (admin/build/modules)
3. Configure:
    Administer > Site configuration > Imagesets > Menuicons (admin/settings/imageset/menuicons)
4. Configure menu icons:
    Administer > Site building > Menu > Edit menu item > Section "Menu Icon" (admin/build/menu)
----------------------------------------------

Configuration
-----------------------------------------------

There are two variants of a output of the menu with pictures imageset
a) Icons (graphic icon + Description)
b) Buttons (graphic button)

Variants of connection Administer> Site configuration> Imagesets> Menuicons (admin/settings/imageset/menuicons)
1. Definition of the menu in the block (for each block of the standard menu the block with a mark (menuicon) which then can be inserted in necessary region will be created)
2. To include themed a output of item of the menu 'menu_item_link' (icons will be it is deduced in all existing blocks of the menu)
