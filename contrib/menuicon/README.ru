
Install
-----------------------------------------------
1. Decompress the file into your Drupal modules directory (usually sites/all/modules).
2. Enable the Menuicon module (This module depends on module Imageset):
    Administer > Site building > Modules (admin/build/modules)
3. Configure:
    Administer > Site configuration > Imagesets > Menuicons (admin/settings/imageset/menuicons)
4. Configure menu icons:
    Administer > Site building > Menu > Edit menu item > Section "Menu Icon" (admin/build/menu)
----------------------------------------------

Description
-----------------------------------------------

Имеется два варианта вывода меню с картинками imageset
а) иконки (графический значок + Надпись)
б) кнопки (грфическая кнопка)

Варианты подключения Administer > Site configuration > Imagesets > Menuicons (admin/settings/imageset/menuicons)
1. Определение меню в блоке (для каждого блока стандартного меню будет создан блок с пометкой (menuicon), которые затем можно вставить в необходимом регионе)
2. Включить темизацию вывод пункта меню menu_item_link (иконки будут выводится во всех существующих блоках меню)
