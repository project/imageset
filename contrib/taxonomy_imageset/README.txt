
Install
-----------------------------------------------
1. Decompress the file into your Drupal modules directory (usually sites/all/modules).
2. Enable the taxonomy_imageset module:
    Administer > Site building > Modules (admin/build/modules)
----------------------------------------------
Description
-----------------------------------------------
This module is shared with modules imageset and taxonomy_image.

Redefinition (theming) functions theme_image is used phptemplate_image

-----------------------------------------------
Settings
-----------------------------------------------
1. Loaded necessary imagesets 
2. We set necessary behaviour for imagesets
3. For necessary terms (mysite/admin/content/taxonomy/NN) in field Imageset it is specified necessary a picture from sets in a format name:index, where name - a name of a set, index - number of a picture in a set

-----------------------------------------------
Restrictions
-----------------------------------------------
It is impossible to scale a picture (mysite/admin/settings/taxonomy_image) Resizing Options only Disable
