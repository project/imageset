
var BUEicon = {

  overrided: false,
  wload: undefined,
  
  override: function () { // drupal6
    if (!window.editor || BUEicon.overrided) return;
    
    var old_theme = editor['theme'];
    if (old_theme) { // identity D6
      editor['theme'] = function() {
        // call old method
	    var result = old_theme.apply(this, arguments);
        result = BUEicon.theme(arguments[0], result);
	    return result;
      }
    }
    // for D5
    var old_template = editor['template'];
    if (old_template) { // identity D5
      editor['template'] = function() {
        // call old method
	    var result = old_template.apply(this, arguments);
        result = BUEicon.template(result);
	    return result;
      }
    }
    BUEicon.overrided = true;
    return;
  },
  
  is_iefix: function (aimageset_name) {
    var userAgent = navigator.userAgent.toLowerCase();
    var _fix = /msie (5\.5)|(6\.0)/.test( userAgent ) && !/opera/.test( userAgent );      
    if (_fix) {
      _fix = typeof(aimageset_name) == 'undefined' || (window.fixie_imagesets !== undefined && fixie_imagesets.concat(',').indexOf(aimageset_name + ',') != -1);
    }
    return _fix;
  },

// overrided BUE.theme (Drupal6)
  theme: function(tplid, html) {
    var re = /<input [^>]*type="button" [^>]*class="([\w\s-]+)" [^>]*value="(\w+):(\d+)" [^>]*\/>/ig;
    return html.replace(re, BUEicon.theme_replace_input);
  },
  theme_replace_input: function (result, aclass, aimageset, aindex) {
    return BUEicon.html_replace(result,aclass, aimageset, aindex);
  },
  
// overrided BUE.template (Drupal5)
  template: function(html) {
    var re = /<input [^>]*type="button" [^>]*value="(\w+):(\d+)" [^>]*class="([\w\s-]+)" [^>]*\/>/ig;
    return html.replace(re, BUEicon.template_replace_input);
  },
  template_replace_input: function (result, aimageset, aindex, aclass) {
    return BUEicon.html_replace(result,aclass, aimageset, aindex);
  },

  html_replace: function(html, aclass, aimageset, aindex) {
    var img_class = 'editor-image-button imageset is-'+ aimageset +' is-' + aimageset + '-' + aindex;
    var img_src = path_imageset + '/1px.png';
    aclass = aclass.replace(/editor-text-button/i, '') + ' ' +img_class;
    html = html.replace(/type="button"/i, 'type="image"');
    html = html.replace(/class="([\w\s-]+)"/i, 'class="' + aclass + '"');
    html = html.replace(/value="[^"]+"/i, '');
    html = html.replace(/\/>/i, 'src="' + img_src + '"/>');
    
    if (this.is_iefix(aimageset)) {
      html = html.replace(/class="([\w\s-]+)"/i, 'class="$1 is-pngfixiebk"');
      html = "<span class=\"editor-image-button\" style=\"overflow:hidden;position:relative;display:inline-block\">" + html + "</span>";
    }
    return html;
  }

}

BUEicon.override();

$(function() {BUEicon.override()})

//order loaded  for Drupal5:  bueditoricon.js bueditor.js
/*
if (document.getElementsByTagName && document.createElement  && document.getElementById && !BUEicon.overrided) {
  BUEicon.wload = window.onload;
  window.onload = (typeof(BUEicon.wload)=='function' ? function(e) {BUEicon.wload(e); BUEicon.override();} : BUEicon.override);
}
*/