
Install
-----------------------------------------------
1. Decompress the file into your Drupal modules directory (usually sites/all/modules).
2. Enable the Bueditorcon module (This module depends on module Imageset and Bueditor):
    Administer > Site building > Modules (admin/build/modules)
----------------------------------------------

Configuration
-----------------------------------------------

1. We load the prepared imageset Administer > Site configuration > Imagesets

2. We set selectors behaviour (at least one) Administer > Site configuration > Imagesets > Selectors
(the Note: For Outer name in this case to set "Bueditor icon")

3. In options of editor Administer > Site configuration > BUEditor > Edit (/admin/settings/bueditor/<N>) it is specified in field Icon value Caption and enter value in a format <name>:<index>, where <name> - a name of an existing set, <index> an index (number) of a picture in a set
