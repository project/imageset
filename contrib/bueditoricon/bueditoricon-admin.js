
$(function() {
  bueicon_editorAdminInitiate();
  }
)

function bueicon_editorAdminInitiate() {
  $('#button-table input').each(function () {
    if (this.id.substr(this.id.length-8) == '-caption') {
      $(this)
        .attr('size', '20')
        .keyup(function () { bueicon_changeIcon(this); });
      bueicon_changeIcon(this);
	}
  });  
  $('#button-table select').each(function () {	
    if (this.id.substr(this.id.length-5) == '-icon') {
      $(this).change(
        function () {
          var suffix = this.id.split('-')[2] +'-caption';
          var input = $('#edit-button-' + suffix + ',#edit-buttons-' + suffix); //for D5, D6
          if (input.length > 0) {
            bueicon_changeIcon(input[0]);
          }
        }
      );
    }
  });
}

function bueicon_changeIcon(input) {
  $(input).parent().find('.editor-image-button').remove();
  var cap = $('#edit-button-'+ $(input).attr('id').split('-')[2] +'-icon');
  if (!cap.attr('value') && (value = $(input).attr('value'))) {
    if (values = value.match(/^(\w+):(\d+)$/i)) {
      var img_class = 'editor-image-button is-'+values[1]+' is-'+values[1]+'-'+values[2];
      $(input).after('<input type="image" src="' + path_imageset + '/1px.png' + '" class="'+ img_class +'" style="display:block;">');
    }
  }
}

