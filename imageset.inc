<?php


/*
 * array(
 *  [version] => version number
 *  [list] => array()
 *  )
 */
function _imageset_get_settings_pre_0() {

  $settings = variable_get('imageset', array());
  if ( (is_array($settings)) and (isset($settings['version']) && !is_array($settings['version'])) ) {
    $version_vars = $settings['version'];
    $result = $settings['list'];
  } 
  else {
    $version_vars = 0;
    $result = $settings;
  }
  return $result;
}


/**
 * function return  returns properties imageset
 * @param
 *   $name - name imageset, if NULL then all imagesets
 * 
 * @result
 *   arrray imagesets
 */

function imageset_get_settings($name = NULL) {
static $settings;

  if (_imageset_version() <= VERSION_IMAGESET_VAR_PRE_0) {
    return _imageset_get_settings_pre_0();
  }

  if (_imageset_version() != VERSION_IMAGESET_VAR) {
    imageset_update_version();
  }

  if (!isset($settings)) {
    $settings = array();
    $result = db_query("SELECT name, data FROM {imageset_data} WHERE type = %d ORDER BY name", IS_TYPE_IMAGESET);
    while ($row = db_fetch_array($result)) {
      $iname = $row['name'];
      $settings[$iname] = unserialize($row['data']);
      if ($settings[$iname]['fullsizex'] == 0) {
        $settings[$iname]['fullsizex'] = $settings[$iname]['sizex'];
      }
      if ($settings[$iname]['fullsizey'] == 0) {
        $settings[$iname]['fullsizey'] = $settings[$iname]['sizey'];
      }
    }  
  }
  if (isset($name)) {
    return $settings[$name];
  }  
  else return $settings;
}

function imageset_set_settings_pre_0($name, $list = array()) {
// backup pre imageset 
  if (db_result(db_query("SELECT count(*) AS cnt FROM {imageset_data} WHERE type = %d AND name = '%s'", IS_TYPE_IMAGESET_PRE_0, $name)) == 0) {
    db_query("INSERT INTO {imageset_data} (type, name, data) VALUES (%d, '%s', '%s')", IS_TYPE_IMAGESET_PRE_0, $name, serialize($list));
  }
  else db_query("UPDATE {imageset_data} SET data = '%s' WHERE type = %d AND name = '%s'", serialize($list), IS_TYPE_IMAGESET_PRE_0, $name);
  
}

function _imageset_version() {

  $settings = variable_get('imageset', array());
  return $settings['version'];
}

function imageset_update_version() {

  imageset_clear_css();
// update version
  $settings = variable_get('imageset', array());  
  $settings['version'] = VERSION_IMAGESET_VAR;
  variable_set('imageset', $settings);  
}

function imageset_names($include_all = FALSE) {

  $result = array();  
  $settings = imageset_get_settings();
  foreach ($settings as $name => $value) {
    $result[$name] = $name;
  }
  if ($include_all) {
    array_unshift($result, t('--- All imagesets ---'));
  }
  return $result;
}

/*
 $size - string "WxH"
 return array ('sizex' => W, 'sizey' => H)
 */
function _imageset_explode_size($size) {

  $v = explode('x', $size);
  return array(
    'sizex' => intval($v[0]),
    'sizey' => intval($v[1]),
  );
}

/*
 * @param $name - imageset name
 * @param $check_css - if TRUE generic CSS
 * @param $outname - format generic CSS 
 * @result array
 ['imgname']	- path to imageset
 ['sizex'] 	- width map
 ['sizey'] 	- height map
 ['vh'] 	- orientation
 ['dis']        - disabled
 ['fixpngie']   - fix alpha chanel for IE
 */
/* 
function _imageset_imageset($name, $check_css = FALSE, $outheme = NULL) {
global $_imageset_used;

  if (!isset($_imageset_used)) {
    $_imageset_used = array();
  }
  if (!isset($_imageset_used[$name])) {
    $v = imageset_get_settings();
    $_imageset_used[$name] = $v[$name];
  }
  $img = $_imageset_used[$name];
  // include CSS
  if ($img && !$img['check_css_'. $outheme] && $check_css) {
    if (!$img['dis']) {
      imageset_insert_css($name, $outheme);
      if ($img['fixpngie']) imageset_init_pngfixie();
    }
    $_imageset_used[$name]['check_css_'. $outheme] = TRUE;
  }
  return $img;
}
*/

/*
 NULL - delete
 */
function _imageset_imageset_update_pre_0($name, $newvalue = NULL) {
//global $_imageset_used;

  $v = imageset_get_settings();
  $save_imgname = $v[$name]['imgname'];
  if (!isset($newvalue['name']) || (isset($newvalue['name']) && $name != $newvalue['name'])) {  // change name		
    unset($v[$name]);
  }
// reset cashe	
//  unset($_imageset_used[$name]);

  if (isset($newvalue['name'])) {
    $name = $newvalue['name'];
    unset($newvalue['name']);
  }
  if (isset($newvalue)) {
    $v[$name] = $newvalue;
  }
  ksort($v);
  imageset_set_settings($v);

  if ($save_imgname && !isset($newvalue)) {
    if (dirname($save_imgname) == file_create_path('imagesets')) {
      file_delete($save_imgname);
    }
  }
}

function _imageset_imageset_update($name, $newvalue = NULL) {
//global $_imageset_used;

  if (_imageset_version() <= VERSION_IMAGESET_VAR_PRE_0) {
    return;
  }
  
  $is_new = FALSE;
  $is_del = !isset($newvalue);
  $img = imageset_get_settings($name);
  if (!isset($img)) $is_new = TRUE;
// reset cashe	
//  unset($_imageset_used[$name]);
  
  if ($is_del) { // delete imageset
    db_query("DELETE FROM {imageset_data} WHERE type=%d AND name='%s'", IS_TYPE_IMAGESET, $name);
    if (isset($img)) {
      $save_imgname = $img['imgname'];
      if ($save_imgname) {
        if (dirname($save_imgname) == file_create_path('imagesets')) {
          file_delete($save_imgname);
        }
      }
    }
  } 
  else {
    $newname = $name;
    if (isset($newvalue['name'])) {
      $newname = $newvalue['name'];
      unset($newvalue['name']);
    }
    if ($is_new) { // insert imageset
      db_query("INSERT INTO {imageset_data} (type, name, data) VALUES (%d, '%s', '%s')", 
        IS_TYPE_IMAGESET, $newname, serialize($newvalue));
    }
    else { // update imageset
      db_query("UPDATE {imageset_data} SET  type = %d, name = '%s', data = '%s' WHERE type =%d AND name = '%s'", 
        IS_TYPE_IMAGESET, $newname, serialize($newvalue), IS_TYPE_IMAGESET, $name);
    }
  }
}

function _imageset_selectors_pre_0($name = NULL) {

  $v = variable_get('imageset_style', array());
  if ($name) {
    $result = array();
    foreach ($v as $style) {
      if ($style['name'] == $name) {
        $result[] = $style;
      }
    }
    if (count($result) == 0) {  // default
      $result[] = array(
        'name' => $name,
        'select' => 1,
        'css' => '',
        'outname' => 'default',
      );
    }
    return $result;
  } 
  else return $v;
}

function _imageset_selectors($name = NULL, $clear = FALSE) {
static $_selectors = FALSE;

  if ($clear) {
    $_selectors = FALSE;
    return;
  }

  if (_imageset_version() <= VERSION_IMAGESET_VAR_PRE_0) {
    return _imageset_selectors_pre_0($name);
  }
  
  if (!is_array($_selectors)) {
    $_selectors = array();
    $_names = array();
    $result = db_query("SELECT isid, name, data FROM {imageset_data} WHERE type = %d ORDER BY name", IS_TYPE_SELECTOR);
    while ($row = db_fetch_array($result)) {
      if (isset($_names[$row['name']])) continue;
      $_names[$row['name']] = $row['isid'];
      
      $items = unserialize($row['data']);
      $npp = 1;
      foreach ($items as $value) {
        $value['npp'] = $npp;
        $_selectors[] = $value;
        $npp++;
      }
    }  
  }
  if (isset($name)) {
    $result = array();
    foreach ($_selectors as $item) {
      if ($item['name'] == $name) {
        $result[] = $item;
      }
    }
    return $result;
  } 
  else return $_selectors;
}

function _imageset_selector_npp($aname, $anpp) {

  $selectors = _imageset_selectors($aname);
  foreach ($selectors as $value) {
    if ($value['npp'] == $anpp) {
      return $value;
    }
  }
} 

function _sort_imageset_selector($val1, $val2) {

  $result = strcasecmp($val1['name'], $val2['name']);
  if ($result == 0) {
    $result= ($val1['weight'] < $val2['weight'])? -1: (($val1['weight'] > $val2['weight'])? 1: 0);
  }
  if ($result == 0) {
    $result = strcasecmp($val1['css'], $val2['css']);
  }
  return $result;
}
/*
 @param $name - imageset name
 @param $npp - number under the order in the block $name ($npp == 0 add element)
 @param $value - array(
	'name' - name imageset
	'select' - index for select 1...n
	'css' 		- CSS selector prefix
	'outname' 		- Outer name
	'weight'	- weight position in CSS
	)
 if (value == NULL) DELETE
 */
function _imageset_selector_update($name, $npp, $value = NULL) {

  if (_imageset_version() <= VERSION_IMAGESET_VAR_PRE_0) {
    return;
  }  

  $v = _imageset_selectors($name);
  
  $is_new = db_result(db_query("SELECT COUNT(*) AS num FROM {imageset_data} WHERE type = %d AND name = '%s'", IS_TYPE_SELECTOR, $name));
  
  if ($npp > 0) {
    if (isset($value)) {
      $v[$npp - 1] = $value;
    }
    else unset($v[$npp - 1]);
  }
  else $v[] = $value;
  usort($v, _sort_imageset_selector);
  if ($is_new == 0) {
    db_query("INSERT INTO {imageset_data} (type, name, data) VALUES (%d, '%s', '%s')", 
      IS_TYPE_SELECTOR, $name, serialize($v));
  }
  else {
    if (count($v) > 0) {
      db_query("UPDATE {imageset_data} SET data = '%s' WHERE type = %d AND name = '%s'", serialize($v), IS_TYPE_SELECTOR, $name);
    }  
    else db_query("DELETE FROM {imageset_data} WHERE type = %d AND name = '%s'", IS_TYPE_SELECTOR, $name);
  }    
// clear cache selectors  
  _imageset_selectors(NULL, TRUE);
}

function _imageset_selector_delete($name) {

  if (_imageset_version() <= VERSION_IMAGESET_VAR_PRE_0) {
    return;
  }
  
  db_query("DELETE FROM {imageset_data} WHERE type = %d AND name = '%s'", IS_TYPE_SELECTOR, $name);
// clear cache selectors  
  _imageset_selectors(NULL, TRUE);
}

function install_imageset($modulename, $filename_dat, &$buffer = NULL) {
  
  imageset_clear_css();
// first string - description
// name, filename, WxH, orientation,default outname; CSS selectors
  $file = fopen($filename_dat, 'r');
  if ($file) {
    $data = fgetcsv($file, 0, ';');
    $items = explode(',', $data[0]);
    $name = $items[0];
    $imgname = $items[1];
    if (dirname($imgname) == '' || dirname($imgname) == '.') {
      $dir = dirname($filename_dat);
      if ($dir == '') $dir = getcwd ();
      $imgname = $dir .'/'. $imgname;
    }
    $value = array_merge(
      array(
        'imgname' => $imgname,
        'vh' => intval($items[3]),
        'module' => $modulename,
      ),
      _imageset_explode_size($items[2])
    );
    $replace_index = 0;
    while (imageset_get_settings($name)) {
      if (!$replace_index) {
        if (preg_match("/_(\d)+$/", $name, $matches)) {
          $replace_index = $matches[1];
        } 
        else $name .= '_0';
      }
      $replace_index++;
      $name = preg_replace('/_(\d)+$/', '_'. $replace_index, $name);
    }
    _imageset_imageset_update($name, $value);
    $outname = $items[4];
    $select = 1;
    while ($select< count($data)) {
      $value = array(
        'name' => $name,
        'select' => $select,
        'css' => $data[$select],
        'outname' => $outname,
        'weight' => $select - 1,
      );
      _imageset_selector_update($name, 0, $value);
      $select++;
    }
    if (func_num_args()>2) { 
      $buffer = array();
      while ($buffer[] = fgets($file)) { 
      }
    }
    fclose($file);
  }
  return $name;
}

function uninstall_imageset($modulename) {

  imageset_clear_css();
  $v = imageset_get_settings();
  foreach ($v as $name => $img) {
    if ($img['module'] == $modulename) {
      _imageset_imageset_update($name);
      _imageset_selector_delete($name);
    }
  }
}

function imageset_clear_css() {
  $v = variable_get('imageset_css', array());
  if (_imageset_version() < 5) {
    foreach ($v as $name_css => $value) {
      file_delete(file_create_path('imagesets') .'/'. $name_css);
    }
  }  
  foreach ($v as $name => $value) {
    if ($value['css']) {
      file_delete(file_create_path('imagesets') .'/'. $name . '.css');
    }  
    if ($value['fixie']) {
      file_delete(file_create_path('imagesets') .'/'. $name . '.fixie.css');
    }  
  }
  variable_del('imageset_css');
  drupal_clear_css_cache();
}

function _imageset_prepare_append_selectors(&$selectors, $style, $max_index) {

  $variant = $style['select'];
  if ($style['indexes']) {
    $indexes = explode(',', $style['indexes']);
  }
  else {
    $indexes = array();
    for ($i = 1; $i <= $max_index; $i++) {
      $indexes[] = $i;
    }
  }
  $selector = explode(',', $style['css']);
  
  foreach ($indexes as $value_index) {
    $outindex = "$variant-$value_index";
    if (!isset($selectors[$outindex])) {
      $selectors[$outindex] = array(
        'variant' => $variant,
        'prefix' => $selector,
        'index' => $value_index,
      );
    }
    else {
      $selectors[$outindex]['prefix'] = array_merge($selectors[$outindex]['prefix'], $selector);
    }
  }
}

/**
 * function created css
 * @param 
 *   $name - name imageset
 * @param 
 *   $outheme - imageset output theme
 * @param 
 *   $absolute_path - relative path, if TRUE then path to image absolute
 *
 * @result array of css
 *   [ 'css']
 *   ['fixie.*']
 */
function imageset_prepare_css($name, $outheme, $absolute_path = FASLE) {

  $img = imageset_get_settings($name);
  if (!isset($img) || $img['dis']) return;
  
  $css = '';
  $css_fixie_png = '';
  $save_selectors = array();
  
    if ($info = image_get_info($img['imgname'])) {
      switch ($img['vh']) {
        case IS_VERTICAL:
          $mi_index = ceil($info['height'] / $img['fullsizey']);
          $mi_variant = ceil($info['width'] / $img['fullsizex']);
          break;
        case IS_HORIZONTAL:
          $mi_index = ceil($info['width'] / $img['fullsizex']);
          $mi_variant = ceil($info['height'] / $img['fullsizey']);
          break;
      }
      $selectors = array();
      $styles = _imageset_selectors($name);
      foreach ($styles as $style) {
        if (_imageset_selector_outname($style) != $outheme) continue;
        
        _imageset_prepare_append_selectors($selectors, $style, $mi_index);
        $save_selectors["$name-$outheme"] = array('name' => $name, 'outheme' => $outheme);
      }
      if (count($selectors) == 0) { // if empty selectors (not fill)
        _imageset_prepare_append_selectors($selectors, array('select' => 1, 'css' => ''), $mi_index);
        $save_selectors["$name-$outheme"] = array('name' => $name, 'outheme' => $outheme);
      }
      
        foreach ($selectors as $outindex => $value) {
          $variant = $value['variant'];
          $index = $value['index'];
          
          switch ($img['vh']) {
            case IS_VERTICAL:
              $ofsx = -($img['fullsizex'] * ($variant - 1) + $img['ofsx']);
              $ofsy = -($img['fullsizey'] * ($index - 1) + $img['ofsy']);
              break;
            case IS_HORIZONTAL:
              $ofsx = -($img['fullsizex'] * ($index - 1) + $img['ofsx']);
              $ofsy = -($img['fullsizey'] * ($variant - 1) + $img['ofsy']);
              break;
          }
          $css_selector = '';
          $element = imageset_get_csselement($outheme, $name, $index);
          $css_selector_fixie = '';
          $element_fixie = "span.is-$name-$index-fixie";
          if ($value['prefix']) {
            foreach ($value['prefix'] as $line) {
              $line = trim($line);
              $result = preg_replace('/\<is\>/', $element, $line);
              if ($result == $line) {
                $result .= ($result ? ' ': '') . $element;
              }
              $css_selector .= ($css_selector ? ',' : '') . $result;
              //for IE6
              if ($img['fixpngie']) {
                $result = preg_replace('/\<is\>/', $element_fixie, $line);
                if ($result == $line) {
                  $result .= ($result ? ' ' : '') . $element_fixie;
                }
                $css_selector_fixie .= ($css_selector_fixie? ',' : '') . $result;
              }
            }
          }
          $css .= "$css_selector{background-position:${ofsx}px ${ofsy}px;}\n";
          if ($img['fixpngie']) {
            $css_fixie_png .= "$css_selector_fixie{star:expression(IS_PNGFIX.calc_fixie(this,${ofsx},${ofsy},". ($img['sizex']-$ofsx) .",". ($img['sizey']-$ofsy) ."))}\n";
          }
        }
    }
  $css_url = '';
  $css_url_fixie_png = '';
  $css_url_fixie_datauri = '';
  $path_css = file_create_path('imagesets');
  $count_root = explode('/', $path_css);
  foreach ($save_selectors as $value) {
    $datauri = '';
    $img = imageset_get_settings($value['name']);
    if ($info = image_get_info($img['imgname'])) {
      if ($info['file_size'] <= MAX_DATAURI_DEFAULT) {
        $datauri = "data:${info['mime_type']};base64,". base64_encode(file_get_contents($img['imgname']));
      }      
    }
    $uri = '';
    if ($absolute_path) {
      $uri = base_path() . $img['imgname'];
    }
    else {
      $path = dirname($img['imgname']);
      if ($path == $path_css) { //current
        $uri = basename($img['imgname']);
      }
      else {
        $uri = $img['imgname'];
        for ($i = 1; $i <= count($count_root); $i++) {
          $uri = '../'. $uri;
        }  
      }  
    }
    $element = imageset_get_csselement($value['outheme'], $value['name']);    
    $css_url .= "$element{width:${img['sizex']}px;height:${img['sizey']}px;background:url(" .($datauri?$datauri:$uri) .") no-repeat transparent;}\n";    
    if ($datauri) {  // hack for pre IE 7
      $css_url_fixie_datauri .= "$element{background-image:url($uri) !important;}\n";
    }      
    if ($img['fixpngie']) {
      $element_fixie_png = "span.is-$name-fixie";
      $css_url_fixie_png .= "$element_fixie_png{position:relative;display:inline-block;background:transparent;}\n"; 
    }
  }
  return array(
    'css' => $css_url . $css, 
    'fixie.png' => $css_url_fixie_png . $css_fixie_png,
    'fixie.datauri' => $css_url_fixie_datauri,
  );
}
