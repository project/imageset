var IS_PNGFIX = window.IS_PNGFIX || {};

IS_PNGFIX.fused_fix = undefined;  

IS_PNGFIX.used_fix = function() {  
    if (this.fused_fix == undefined) {
      var userAgent = navigator.userAgent.toLowerCase();
      this.fused_fix = /msie (5\.5)|(6\.0)/.test( userAgent ) && !/opera/.test( userAgent );      
    }
    return this.fused_fix;
};
  
IS_PNGFIX.get_style = function (element, name) {
    if (element.currentStyle[name]) {
      return name + ':'+ element.currentStyle[name]+';';
    }
    else return '';
};

IS_PNGFIX.get_style_obj = function (obj, name) {
    if (obj.css(name)) {
      return {name: obj.css(name)};
    }
    else return {};
};
  
IS_PNGFIX.get_style_border = function (element) {
    var result = '';
    result += 'border-style:' +element.currentStyle.borderStyle +';';
    result += 'border-width:' +element.currentStyle.borderWidth +';';
    result += 'border-color:' +element.currentStyle.borderColor +';';
    return result;
};

IS_PNGFIX.get_style_margin = function (elemen) {
    var result = '';
    result += this.get_style(elemen, 'margin');
    return result;
};

IS_PNGFIX.get_style_padding = function (elemen) {
    var result = '';
    result += this.get_style(elemen, 'padding');
    return result;
};

IS_PNGFIX.fix = function (element) {

    if (!this.used_fix()) return;
    if (element.tagName != 'IMG') return;
    if (element.currentStyle.display == 'none') return;    
    element.runtimeStyle.filter = '';
    
    var url = element.currentStyle.backgroundImage;    
    var src = url.match(/^url\(['"]?(.*\.png)['"]?\)$/i);
    if (!src) return;      
    src = src[1];      
    var width = parseInt(element.currentStyle.width);
    var height = parseInt(element.currentStyle.height);
      
    var imgId = '';
    var imgClass = '';

    var result = element.className.match(/is-[\S]+/ig);
    for (var i = 0; i < result.length; ++i) {
      imgClass += result[i]+'-fixie ';
    }
    imgClass = (imgClass ? 'class="'+imgClass+'" ':'');
      
    var imgTitle = element.title;
    imgTitle = (imgTitle ? 'title="' + imgTitle + '" ' : '');

    var imgAlt = element.alt;
    imgAlt =  (imgAlt ? 'alt="' + imgAlt + '" ' : '');
	  
    var imgHand = (element.parentNode['href'] != undefined) ? 'cursor:hand;' : '';
      
    var imgVAlign = element.currentStyle.verticalAlign;
    imgVAlign = (imgVAlign ? 'vertical-align:'+imgVAlign+';' : '');
      
    var imgAlign = '';
    var imgStyle = '';	  
    //var imgAlign = (img.attr('align')) ? 'float:' + img.attr('align') + ';' : '';	  
      
    var prevStyle = '';
    prevStyle += this.get_style_border(element);
    prevStyle += this.get_style_padding(element);
    prevStyle += this.get_style_margin(element);

    //var imgStyle = (img[0].style.cssText);
      
    var strNewHTML = '';
    strNewHTML += '<span '+imgId+imgClass+imgTitle+imgAlt;
    strNewHTML += 'style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader' + '(src=\'' + src + '\', sizingMethod=\'crop\');"';
    strNewHTML += '></span>';
	//strNewHTML = '<span style="position:relative;overflow:hidden;display:inline-block;'+ imgHand + imgVAlign + 
	//  'width:' + (width) + 'px;' + 'height:' + (height) + 'px;'+prevStyle+'">' + strNewHTML + '</span>';
    var span = document.createElement("span");
    span.style.cssText = 'position:relative;overflow:hidden;display:inline-block;'+ imgHand + imgVAlign + 'width:' + (width) + 'px;' + 'height:' + (height) + 'px;'+prevStyle;
    span.innerHTML = strNewHTML;
    //img.after(strNewHTML);
    element.parentNode.insertBefore(span , element.nextSibling )
    //img.hide();
    element.style.display = 'none';
};

IS_PNGFIX.calc_fixie = function(element, aleft, atop, awidth, aheight) {
  with (element.runtimeStyle) {
    if (aleft != parseInt(left)) left = aleft + 'px';
    if (atop != parseInt(top))   top = atop + 'px';
    if (awidth != parseInt(width))   width = awidth + 'px';
    if (aheight != parseInt(height)) height = aheight + 'px';
  }
};
  
