
Install
-----------------------------------------------
1. Decompress the file into your Drupal modules directory (usually sites/all/modules).
2. Enable the imageset module:
    Administer > Site building > Modules (admin/build/modules)
3. Configure access rules:
    Administer > User management > Access control (admin/user/access)
4. Configure:
    Administer > Site configuration > Imagesets (admin/settings/imageset)

----------------------------------------------

Description
-----------------------------------------------

Imageset a means of automated process output image files using technologies CSS sprites.
By combining graphics files to the server receives a substantial savings in the amount of data transferred and the number of HTTP connections. For the client side also decreases traffic and load time.


In a basis of implementation it module Imageset, and in addition module Menuicon (icons for the menu) and module Bueditoricon (icons for the editor bueditor).


-----------------------------------------------

Last change
-----------------------------------------------

1.6
- Added support for technology datauri for small (less than a specified size) of files
- Optimized process of themed imageset. Access to the database occur only once when creating CSS.
- Added ability to set personal selectors for individual images.

1.5
- add the ability to set the offset in the shown area sprite
- add translate russian for D5

