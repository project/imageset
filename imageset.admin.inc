<?php

require_once 'imageset.inc';


function _imageset_orientation_name($i = NULL, $type = 'view') {

  switch ($type) {
    case 'view': 
      $vh = array(IS_VERTICAL => t('Vertical'), IS_HORIZONTAL => t('Horizontal'));
      break;
  }
  if (isset($i)) {
    return $vh[$i];
  } 
  else return $vh;
}

function _imageset_imageset_edit($value = array()) {

  $is_new = empty($value);
  $form = array();
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $value['name'],
    '#description' => t('The machine-readable name of this image set. This text will be used for constructing the CSS of selector. This name may consist of only of lowercase letters, numbers, and underscores.'),
    '#required' => TRUE,
  );
  if (!$is_new) {
    $form['dis'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disabled'),
      '#default_value' => $value['dis'],
    );
  }
  $form['persistent'] = array(
    '#type' => 'checkbox',
    '#title' => t('Persistent'),
    '#default_value' => $value['persistent'],
    '#description' => t('Always to connect CSS to page. Usually CSS it is connected at explicit use Imageset on page, ex. theme ("imageset",). It is recommended if imageset it is used only in javascript.'),
  );
  $form['fixpngie'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fix PNG for IE5.5-6'),
    '#default_value' => $value['fixpngie'],
    '#description' => t('To use for support imageset with the alpha-channel (transparency) for IE5.5, IE6.'),
  );
  if ($is_new) {
    $form['path_upload'] = array(
      '#type' => 'file',
      '#title' => t('Path for imageset'),
      '#size' => 60,
    );
  }
  else {
    $form['imgname'] = array(
      '#type' => 'textfield',
      '#title' => t('Path for imageset'),
      '#default_value' => $value['imgname'],
      '#description' => t('Path to the location of a file imageset'),
      '#disabled' => TRUE,
    );
  }
  $default_value_size = ($is_new ? '16x16' : sprintf('%dx%d', $value['sizex'], $value['sizey']));
  $form['size'] = array(
    '#type'		=> 'textfield',
    '#title'		=> t('Shown size (Width and Height)'),
    '#default_value'	=> $default_value_size,
    '#description'	=> t('The size of shown area in pixels as {width}x{height}. For example: 16x16'),
    '#required'	=> TRUE, 
  );

  $default_value_fullsize = '';
  if (!($is_new || ($value['fullsizex'] == 0 && $value['fullsizey'] == 0))) {
    $default_value_fullsize = sprintf('%dx%d', $value['fullsizex'], $value['fullsizey']);
  }  
  if ($default_value_fullsize == $default_value_size) {
    $default_value_fullsize = '';
  }
  $default_value_offset = '';
  if (!($is_new || ($value['ofsx'] == 0 && $value['ofsy'] == 0))) {
    $default_value_offset = sprintf('%dx%d', $value['ofsx'], $value['ofsy']);
  }
  
  $form['size_ext'] = array(
    '#type' => 'fieldset',
    '#title' => t('Optional size'),
    '#description' => t('If there are gaps between the visible parts of you must specify the full size pictures and offset.'),
    '#collapsible' => TRUE,
    '#collapsed' => ($default_value_fullsize == '' && $default_value_offset == ''),
  );
  $form['size_ext']['sample_img'] = array(
    '#type' => 'item',
    '#value' => theme('image', drupal_get_path('module', 'imageset') .'/sprite-size.png'),
  );
  $form['size_ext']['fullsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Full size (SizeW and SizeH)'),
    '#default_value' => $default_value_fullsize,
    '#description' => t('The size of area in pixels as {sizeW}x{sizeH}. If to specify empty a line that the size of shown area will be used'),
    '#required'	=> FALSE, 
  );  
  $form['size_ext']['offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Offset (OfsW and OfsH)'),
    '#default_value' => $default_value_offset,
    '#description' => t('Offset of shown area as {OfsW}x{OfsH}. By default value 0'),
    '#required'	=> FALSE, 
  );
  $form['vh'] = array(
    '#type' => 'radios',
    '#title' => t('Orientation'),
    '#default_value' => ($value['vh'] ? IS_HORIZONTAL : IS_VERTICAL),
    '#options' => _imageset_orientation_name(),
  );
/*  
  if (!$is_new) {
    $form['sample'] = array(
      '#type' => 'fieldset',
      '#title' => t('Sample'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    
    $form['sample']['index'] = array(
      '#type' => 'textfield',
      '#title' => t('Index'),
      '#size' => 6,
      '#maxsize' => 6,
      '#default_value' => '1',
    );
    $form['sample']['item'] = array(
      '#type' => 'item',
      '#value' => theme('imageset', $value['name'], 1),
    );
  }  
 */  
  return $form;
}

function imageset_admin_settings() {
  $form = array();
  $form['clear'] = array('#type' => 'submit', '#value' => t('Clear cache CSS'), '#name' => 'clear' );
  return $form;
}

function imageset_admin_settings_submit($form_id, $form_value) {
  if ($form_value['clear']) {
    imageset_clear_css();
  }
}

function imageset_admin_settings_imageset() {

  $is_edit = (_imageset_version() == VERSION_IMAGESET_VAR);
  
  $settings = imageset_get_settings();
  $form = array();
  $form['#attributes'] = array("enctype" => "multipart/form-data");
  $header = array(t('Name'), t('Path for imageset'), t('Size'), t('Orientation'), t('Description'), t('Operation') );
  
  $rows = array();
  foreach ($settings as $name => $value) {
    if ($name) {
      $desc = '';
      $filesize = 0;
      if ($info = image_get_info($value['imgname'])) {
        $filesize = $info['file_size'];
        switch ($value['vh']) {
          case IS_VERTICAL:
            $mi_index = ceil($info['height'] / $value['fullsizey']);
            $mi_select = ceil($info['width'] / $value['fullsizex']);
            break;
          case IS_HORIZONTAL:
            $mi_select = ceil($info['height'] / $value['fullsizey']);
            $mi_index = ceil($info['width'] / $value['fullsizex']);
            break;
        }
        $desc = sprintf('images: %d, variants: %d, filesize: %s', $mi_index, $mi_select, $filesize);
        if ($value['fixpngie']) {
          $desc .= sprintf(' (%s)', t('Fix PNG for IE5.5-6'));
        }
      }
      $size = sprintf('%dx%d', $value['sizex'], $value['sizey']);
      $fullsize = sprintf('%dx%d', $value['fullsizex'], $value['fullsizey']);
      $rows[] = array(
        check_plain($name) . ($value['dis'] ? ' ('. t('Disabled') .')' : ($value['persistent'] ? ' ('. t(Persistent) .')' : '')),
        l($value['imgname'], $value['imgname']),
        ($size == $fullsize ? $size : "$fullsize ($size)"),
        _imageset_orientation_name($value['vh']),
        $desc,
        ($is_edit? l(t('Edit'), PATH_IMAGESET_SETTINGS. '/imagesets/edit/'. $name): '').
        '<br />'. l(t('View code'), PATH_IMAGESET_SETTINGS. '/selectors/view/'. $name),
      );
    }
  }  
  
  if ($is_edit) {  
    $form['new'] = array_merge(
      array(
        '#type' => 'fieldset',
        '#title' => t('New imageset'),
        '#collapsible' => TRUE,
        '#collapsed' => (count($rows) > 0),
      ),
      _imageset_imageset_edit()
    );
    $form['new']['submit'] = array('#type' => 'submit', '#value' => t('Save new imageset') );
  }  
  
  $form['rows'] = array(
    '#type' => 'item',
    '#value' => (count($rows) == 0)? t('The empty list of a set of images'): theme('table', $header, $rows),
  );
  return $form; 
}

function _check_imageset_name($form_value) {

  $name = $form_value['name'];
  if ($name == $form_value['org_name']) return;
  
  if (!preg_match('!^[a-z0-9_]+$!', $name)) {
    form_set_error('name', t('The machine-readable name can only consist of lowercase letters, underscores, and numbers.'));
  }
  else {
    $value = imageset_get_settings($name);
    if ($value) {
      form_set_error('name', t('The Imageset with such name already is present.'));
    }
  }
}

/**
 * validate size format {sizex}x{sizey}
 */
function _check_imageset_size($form_value) {

  $size = _imageset_explode_size($form_value['size']);
  if (!$size['sizex'] || !$size['sizey']) {
    form_set_error('size', t('The size can be submitted as {width}x{height}.'));
  }
}

/**
 * validate fullsize format {sizex}x{sizey}
 */
function _check_imageset_fullsize($form_value) {

  $value = $form_value['fullsize'];
  if ($value) {
    $size = _imageset_explode_size($value);
    if (!$size['sizex'] || !$size['sizey']) {
      form_set_error('fullsize', t('The Full Size can be submitted as {SizeW}x{SizeH}.'));
    }  
  }
}

function _check_imageset_offset($form_value) {

  $value = $form_value['offset'];
  if ($value) {
    $size = _imageset_explode_size($value);
    if (!$size['sizex'] || !$size['sizey']) {
      form_set_error('offset', t('The Offset can be submitted as {OfsW}x{OfsH}.'));
    }  
  }
}
function imageset_admin_settings_imageset_validate($form_id, &$form_values) {
  
  $values = _pvb_form_value($form_values);
  
// check Name
  _check_imageset_name($values);
// check Size
  _check_imageset_size($values);
// check FullSize
  _check_imageset_fullsize($values);
// check Offset
  _check_imageset_offset($values);
  
  if (_pvb_is_v6()) { //for D6
    $validators = array(
      'file_validate_is_image' => array(),
//      'file_validate_image_resolution' => array(variable_get('user_picture_dimensions', '85x85')),
      'file_validate_size' => array(variable_get('user_picture_file_size', '30') * 1024),
    );
    $file = file_save_upload('path_upload', $validators);
  }
  else { // for D5
    if ( ($file = file_save_upload('path_upload')) && ($info = image_get_info($file->filepath)) ) {
    } 
    else form_set_error('path_upload', t('Only JPEG, PNG and GIF images are allowed to be used as imageset.'));
  }
  if (!form_get_errors() && $file) {  
    $imagesetpath = file_create_path('imagesets');
    file_check_directory($imagesetpath, FILE_CREATE_DIRECTORY);
    $filename = $imagesetpath .'/'. $file->filename;
    if (file_copy($file, $filename, FILE_EXISTS_RENAME)) {
      _pvb_set_form_value($form_values, 'imgpath', $file->filepath);
    }
    else {
      form_set_error('path_upload', t("Failed to upload the picture image; the %directory directory doesn't exist or is not writable.", array('%directory' => $imagesetpath)));
    }
  }
}

function imageset_admin_settings_imageset_submit($form_id, &$form_values) {

  $values = _pvb_form_value($form_values);
  
  $size = _imageset_explode_size($values['size']); // Width and Height
  $fullsize = _imageset_explode_size($values['fullsize']); // SizeW and SizeH
  $offset = _imageset_explode_size($values['offset']); // OfsW and OfsH
  $result = array(
    'imgname' => $values['imgpath'],
    'vh' => $values['vh'],
    'persistent' => $values['persistent'],
    'fixpngie' => $values['fixpngie'],
    'sizex' => $size['sizex'],
    'sizey' => $size['sizey'],
    'fullsizex' => $fullsize['sizex'],
    'fullsizey' => $fullsize['sizey'],
    'ofsx' => $offset['sizex'],
    'ofsy' => $offset['sizey'],
  );
  _imageset_imageset_update($values['name'], $result);
  imageset_clear_css();
}

/**
 * for D5 ($arg)
 * for D6 ($form_state, $arg)
 */
function imageset_admin_imageset_edit($arg1 = NULL, $arg2 = NULL) {

  if (_pvb_is_v6()) {
    $values = $arg1['values'];
    return do_imageset_admin_imageset_edit($values, $arg2);
  }
  else {
    $values = $_POST;
    return do_imageset_admin_imageset_edit($values, $arg1);
  }
}

function do_imageset_admin_imageset_edit($values, $name = NULL) {
  if (!$name) return;

  $img = imageset_get_settings($name);
  if (!$img) {
    drupal_set_message(t('Imageset @name not found', array('@name' => $name)));
    return;
  }

  $img['name'] = $name;
  $form = array();
  $form['#redirect'] = 'admin/settings/imageset/imagesets';
  $form['org_name'] = array(
    '#type' => 'value',
    '#value' => $name,
  );
  $form['edit'] = array_merge(
    array(
      '#type' => 'fieldset',
      '#title' => t('Imageset'),
      '#collapsible' => FALSE,
    ),
    _imageset_imageset_edit($img)
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save imageset') );
  
  if (!$img['imgname'] || (dirname($img['imgname']) == file_create_path('imagesets')) || !file_exists($img['imgname']) ) {
  
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete imageset'), '#name' => 'op_del');
  }
  return $form;
}

function imageset_admin_imageset_edit_validate($form_id, &$form_value) {
  
  $values = _pvb_form_value($form_value);
// check Name
  _check_imageset_name($values);
// check Size			
  _check_imageset_size($values);
// check FullSize			
  _check_imageset_fullsize($values);
// check Offset
  _check_imageset_offset($values);
}

function imageset_admin_imageset_edit_submit($form_id, &$form_value) {

  $values = _pvb_form_value($form_value);
  $name = $values['org_name'];

  if ($values['op_del']) {  // delete	
    _imageset_imageset_update($name);
  }
  if ($values['op']) {

    $img = imageset_get_settings($name);
    
    $size = _imageset_explode_size($values['size']); // Width and Height
    $fullsize = _imageset_explode_size($values['fullsize']); // SizeW and SizeH
    $offset = _imageset_explode_size($values['offset']); // OfsW and OfsH
    $result = array(
      'name' => $values['name'],
      'imgname' => $img['imgname'],
      'vh' => $values['vh'],
      'dis' => $values['dis'],
      'persistent' => $values['persistent'],
      'fixpngie' => $values['fixpngie'],
      'sizex' => $size['sizex'],
      'sizey' => $size['sizey'],
      'fullsizex' => $fullsize['sizex'],
      'fullsizey' => $fullsize['sizey'],
      'ofsx' => $offset['sizex'],
      'ofsy' => $offset['sizey'],
    );    
    _imageset_imageset_update($name, $result);
  }
  imageset_clear_css();
}

function _imageset_selector_edit($value = array()) {

  $is_new = empty($value);

  $form = array();

  $form['npp'] = array(
    '#type' => 'value',
    '#value' => $value['npp'],
  );
  $form['css'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS selector'),
    '#default_value' => $value['css'],
    '#description' => t('The text which will be used as CSS the selector. It is possible to specify a little separating a divider ",". <em>&lt;is&gt;</em> defines the identifier imageset'),
  );

  $out_names = imageset_get_element_info(NULL, 'name');
  $form['outname'] = array(
    '#type' => 'select',
    '#title' => t('Outer name'),
    '#default_value' => _imageset_selector_outname($value),

//		'#description'	=> t('The machine-readable name of this image set. This text will be used for constructing the CSS of selector. This name may consist of only of lowercase letters, numbers, and underscores. Dashes are not allowed.'),
    '#options' => $out_names,
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => ($value? $value['weight']: '0'),
    '#delta' => 10,
    '#description' => t('Optional. In the CSS, the heavier items will block lighter items.'),
  );
  $form['name'] = array(
    '#type' => 'select',
    '#disabled' => (!$is_new),
    '#title' => t('Imageset'),
    '#default_value' => ($value['name'] ? $value['name'] : _imageset_current_imageset()),
//		'#description'	=> t('The machine-readable name of this image set. This text will be used for constructing the CSS of selector. This name may consist of only of lowercase letters, numbers, and underscores. Dashes are not allowed.'),
    '#options' => imageset_names(),
  );
  $form['indexes'] = array(
    '#type' => 'textfield',
    '#title' => t('Numbers (index) images'),
    '#default_value' => ($value['indexes']),
    '#description' => t('The specified selector will be used for specified numbers of images. Numbers indicate a comma. If nothing is entered it will be applied to all images imageset.'),
  );
  $form['select'] = array(
    '#type' => 'textfield',
    '#title' => t('Select variant'),
    '#default_value' => ($value['select']? $value['select']: '1'),
    '#size' => 6,
    '#maxlength' => 3,
//		'#description'	=> t(''),
    '#required' => !$is_new, 
  );
  return $form;
}

function _view_css_selector($css) {

  $result = '';
  $prefix = explode(',', $css);
  foreach ($prefix as $line) {
    $line = trim($line);
    if (strpos($line, '<is>') === FALSE) {
      $line = ($line ? check_plain($line) .' ' : '') .'<em>&lt;is&gt;</em>';
    } 
    else $line = check_plain($line);
    $result .= ($result ? ',' : '') . $line;
  }
  return $result;
}

function _imageset_current_imageset() {
  $result = $_SESSION['imageset_current_imageset'];
  return ($result? $result: NULL);
}

function imageset_admin_settings_selector_filter_form_submit($form_id, &$form_values) {

  $values = _pvb_form_value($form_values);
  if (isset($values['submit_filter'])) {
    $_SESSION['imageset_current_imageset'] = $values['imageset'];
  }
}

function imageset_admin_settings_selector_filter_form() {

  $form = array();
  $form['imageset_filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Imageset filter'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['imageset_filter']['imageset'] = array(
    '#type' => 'select',
    '#title' => t('Imageset name'),
    '#default_value' => _imageset_current_imageset(),
    '#description' => t('Here can specify imageset a name for restriction of the list.'),
    '#options' => imageset_names(TRUE),
  );
  $form['imageset_filter']['imageset']['#attributes'] = array('onchange' => 'this.form.submit()');
  $form['imageset_filter']['submit_filter'] = array('#type' => 'submit', '#value' => t('Apply'));
  return $form;
}

function imageset_admin_settings_selector_new_form() {

  $settings = _imageset_selectors(_imageset_current_imageset());
  $is_edit = (_imageset_version() == VERSION_IMAGESET_VAR);
  
  $rows = array();
  foreach ($settings as $value) {  
    $outname = imageset_get_element_info(_imageset_selector_outname($value), 'name');
    $rows[] = array(
      _view_css_selector($value['css']),
      check_plain($outname),
      intval($value['weight']),
      sprintf('%s: %s', check_plain($value['name']), check_plain($value['indexes'] ? $value['indexes'] : t('all')) ),
      intval($value['select']),
      ($is_edit ? l(t('Edit'), 'admin/settings/imageset/selectors/edit/'. $value['name'] .'/'. $value['npp']) : ''),
    );
  }
  
  $form = array();
  if ($is_edit) {
    $form['new'] = array_merge(
      array(
        '#type' => 'fieldset',
        '#title' => t('New imageset selector'),
        '#collapsible' => TRUE,
        '#collapsed' => (count($rows) > 0),
      ),
      _imageset_selector_edit()
    );
    $form['new']['submit'] = array('#type' => 'submit', '#value' => t('Save new selector') );
  };
  
  $header = array(t('CSS selector'), t('Outer name'), t('Weight'), t('Imagesets'), t('Variant'), t('Operation') );  
  $form['rows'] = array(
    '#type' => 'item',
    '#value' => (count($rows)? theme('table', $header, $rows): t('Not defined CSS selector values')),
  );
  $form['#validate'] = _pvb_form_func('imageset_admin_settings_selector_validate');
  $form['#submit'] = _pvb_form_func('imageset_admin_settings_selector_submit');
  return $form;
}

function imageset_admin_settings_selector() {

  $output = ''; 
  $output .= drupal_get_form('imageset_admin_settings_selector_filter_form');
  $output .= t('<a href="@generate">Here</a> it is possible to look generated CSS.', 
    array('@generate' => base_path() . PATH_IMAGESET_SETTINGS .'/selectors/view/'. _imageset_current_imageset()));
  $output .= drupal_get_form('imageset_admin_settings_selector_new_form');
  return $output;
}

function _check_imageset_selector($form_value) {

  $css = $form_value['css'];
  if ($css && !preg_match('!^[a-z0-9_\-#.,<>:\! ]+$!', $css)) {
    form_set_error('css', t("This text can contain only lowercase letters, underscores, numbers, and signs '-.,#<>:! "));
  }
}

function _check_imageset_indexes($form_value) {

  $indexes_str = trim($form_value['indexes']);
  if ($indexes_str) {
    $indexes = explode(',', $indexes_str);
    foreach ($indexes as $value) {
      if (!is_numeric($value)) {
        form_set_error('indexes', t('The value must contain only numbers separated by commas. Or an empty string.'));
        break;
      }
    }
  }
}

function imageset_admin_settings_selector_validate($form_id, &$form_values) {

  $values = _pvb_form_value($form_values);
  _check_imageset_selector($values);
  _check_imageset_indexes($values);
}

function imageset_admin_settings_selector_submit($form_id, &$form_values) {

  $values = _pvb_form_value($form_values);
  $npp = $values['npp'];
  $name = $values['name'];

  if ($values['op_del']) {  // delete
    _imageset_selector_update($name, $npp);
  }
  if ($values['op']) { // update
    $result = array(
      'name' => $name,
      'select' => $values['select'],
      'css' => trim($values['css']),
      'outname' => $values['outname'],
      'weight' => $values['weight'],
      'indexes' => trim($values['indexes']),
    );
    _imageset_selector_update($name, $npp, $result);
  }
  imageset_clear_css();
}


/*
 * for D5 ($arg)
 * for D6 ($form_state, $arg)
 */
function imageset_admin_selector_edit($arg1 = NULL, $arg2 = NULL, $arg3 = NULL) {

  if (_pvb_is_v6()) {
    $values = $arg1['values'];
    return do_imageset_admin_selector_edit($values, $arg2, $arg3);
  }
  else {
    $values = $_POST;
    return do_imageset_admin_selector_edit($values, $arg1, $arg2);
  }
}

function do_imageset_admin_selector_edit($values, $aname = NULL, $npp = NULL) {

  if (!isset($aname)) return;
  if (!isset($npp) || !is_numeric($npp)) return;

  $value = _imageset_selector_npp($aname, $npp);
  if (!$value) {
    drupal_set_message(t('Imageset selector "@name @npp" not found', array('@name' => $aname, '@npp' => $npp)));
    return;
  }
  $value['npp'] = $npp;
  
  $form = array();
  $form['#redirect'] = 'admin/settings/imageset/selectors';  
  $form['#validate'] = _pvb_form_func('imageset_admin_settings_selector_validate');
  $form['#submit'] = _pvb_form_func('imageset_admin_settings_selector_submit');
  
  $form['edit'] = array_merge(
    array(
      '#type' => 'fieldset',
      '#title' => t('Imageset style'),
      '#collapsible' => FALSE,
    ),
    _imageset_selector_edit($value)
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save imageset style'), '#name' => 'op' );
  $form['delete'] = array('#type' => 'submit', '#value' => t('Delete imageset style'), '#name' => 'op_del');
  return $form;
}

function imageset_admin_settings_view_code($name = NULL) {

  $imagesets = array();
  if (!isset($name)) {
    $name = _imageset_current_imageset();
  }
  if (isset($name)) {
    $imagesets[] = $name;
  } 
  else return;
  
  $imageset_list = array();
  foreach ($imagesets as $name) {  
    $selectors = _imageset_selectors($name);
    foreach($selectors as  $selector) {
      $outheme = $selector['outname'];
      if (!isset($imageset_list["$name.$outname"])) {
        $imageset_list["$name.$outname"] = array(
          'name' => $name,
          'outheme' => $outheme,
        );
      }      
    }  
  }  
  
  $output = '';
  
  foreach($imageset_list as $value) {
  
    $css = imageset_prepare_css($value['name'], $value['outheme'], TRUE);
    
    $output .= "/*\nPHP code to insert html code imageset ${value['name']} (${value['outheme']})\n \$index - number of pictures in a imageset\n*/\n";
    $attributes = array();
    $html = module_invoke_all('imageset_element', 'out-'. $value['outheme'], $value['name'], '$index', $attributes);
    $output .= "\$output = \"". check_plain(str_replace('"', '\"', reset($html))) ."\";\n";
    
    $output .= "/*\nCSS for imageset ${value['name']} (${value['outheme']})\n*/\n";
    $output .= check_plain($css['css']);
    if ($css['css_fixie']) {
      $output .= "/*\nCSS for imageset ${value['name']} (${value['outheme']}) only pre IE 6\n*/\n";
      $output .= check_plain($css['css_fixie']);
    }  
  }
  $output = str_replace("\n", '<br />', $output);  
  return $output;
}
