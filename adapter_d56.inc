<?php

function _pvb_is_v6() {
static $_is6;

  if (!isset($_is6)) {
    $vers = explode('.', VERSION);
    $_is6 = (intval($vers[0]) == 6);
  }
  return $_is6;
}

function _pvb_set_form_value(&$form_value, $name, $value) {

  if (_pvb_is_v6()) {
    $form_value['values'][$name] = $value;
  } 
  else {
    $GLOBALS['form_values'][$name] = $value; // for D5
  }
}

function _pvb_form_value(&$form_value) {

  if (_pvb_is_v6()) {
    return $form_value['values'];
  } 
  else return $form_value; // for D5
}

/**
 * form validate and submit function
 */
function _pvb_form_func($func, $prev_merge = NULL) {

  if (_pvb_is_v6()) {
    if (is_array($func)) {
      $result = $func;
    } 
    else $result = array($func);
  }
  else {
    if (is_array($func)) {
      $result = array();
      foreach ($func as $value) {
        $result[$value] = array();
      }
    } 
    else $result = array($func => array());
  }
  if (isset($prev_merge)) {
    return array_merge($prev_merge, $result);
  }
  else return $result;
}

function _pvb_l($text, $path, $options = array()) {

  if (_pvb_is_v6()) { // for Drupal6
    return l($text, $path, $options);
  }
  else { // for Drupal5
    // function l($text, $path, $attributes = array(), $query = NULL, $fragment = NULL, $absolute = FALSE, $html = FALSE) {
    $attributes = $options['attributes'];
    $query = $options['query'];
    $fragment = $options['fragment'];
    $absolute = $options['absolute'];
    $html = $options['html'];
    return l($text, $path, $attributes, $query, $fragment, $absolute, $html);
  }
}

/**
 * Drupal5: function hook_form_alter($form_id, &$form) {
 * Drupal6: function hook_form_alter(&$form, $form_state, $form_id) { 
 */
function _pvb_form_alter_header(&$arg1, &$arg2, &$arg3, $form_id, &$form) {

  if (_pvb_is_v6() && $arg3 == $form_id) {
    $form = $arg1;return TRUE;
  }
  if (!_pvb_is_v6() && $arg1 == $form_id) {
    $form = $arg2;return TRUE;
  }
}

function _pvb_language() {
  if (_pvb_is_v6()) {
    global $language;
    return $language->language;
  }
  else {
    global $locale;
    return $locale;
  }
}
